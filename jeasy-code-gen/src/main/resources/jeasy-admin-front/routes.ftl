export default [
  {
    path: '/${table.camelName}',
    title: '${table.comment}',
    name: '${table.camelName}',
    icon: 'ios-book',
    component: () => import('@/app/${table.camelName}')
  }
]
