export default [
  {
    path: '/log',
    title: '操作日志',
    name: 'log',
    icon: 'ios-book',
    component: () => import('@/app/log')
  }
]
