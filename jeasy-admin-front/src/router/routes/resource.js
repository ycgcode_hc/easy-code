export default [
  {
    path: '/resource',
    title: '菜单资源',
    name: 'resource',
    icon: 'android-menu',
    component: () => import('@/app/resource')
  }
]
