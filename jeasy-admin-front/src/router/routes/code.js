export default [
  {
    path: '/api',
    title: '接口文档',
    name: 'api',
    icon: 'usb',
    component: () => import('@/app/code/api')
  },
  {
    path: '/code',
    title: '代码生成',
    name: 'code',
    icon: 'code-download',
    component: () => import('@/app/code')
  }
]
