package com.jeasy.organization.biz;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.jeasy.base.web.dto.ModelResult;
import com.jeasy.common.Func;
import com.jeasy.common.object.BeanKit;
import com.jeasy.common.spring.SpringContextHolder;
import com.jeasy.common.str.StrKit;
import com.jeasy.exception.MessageException;
import com.jeasy.organization.dto.*;
import com.jeasy.organization.entity.OrganizationEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 机构 Biz
 *
 * @author taomk
 * @version 1.0
 * @since 2017/11/08 16:33
 */
@Slf4j
@Component
public class OrganizationBiz {

    public static OrganizationBiz me() {
        return SpringContextHolder.getBean(OrganizationBiz.class);
    }

    public List<OrganizationListResDTO> transferOrganizationListResDTO(List<OrganizationDTO> organizationDTOList) {
        Map<Long, OrganizationListResDTO> organizationListResDTOMap = Maps.newHashMap();
        if (Func.isNotEmpty(organizationDTOList)) {
            for (OrganizationDTO organizationDTO : organizationDTOList) {
                OrganizationListResDTO organizationListResDTO = new OrganizationListResDTO();
                organizationListResDTO.setId(organizationDTO.getId());
                organizationListResDTO.setTitle(organizationDTO.getName());
                organizationListResDTO.setPid(organizationDTO.getPid());
                organizationListResDTOMap.put(organizationDTO.getId(), organizationListResDTO);
            }
        }

        List<OrganizationListResDTO> organizationListResDTOList = Lists.newArrayList();
        for (OrganizationDTO organizationDTO : organizationDTOList) {
            OrganizationListResDTO organizationListResDTO = organizationListResDTOMap.get(organizationDTO.getId());
            if (Func.isNullOrZero(organizationDTO.getPid())) {
                organizationListResDTOList.add(organizationListResDTO);
            } else {
                OrganizationListResDTO parentOrganizationListResDTO = organizationListResDTOMap.get(organizationDTO.getPid());
                List<OrganizationListResDTO> children = parentOrganizationListResDTO.getChildren();
                if (children == null) {
                    children = Lists.newArrayList();
                    parentOrganizationListResDTO.setChildren(children);
                }
                children.add(organizationListResDTO);
            }
        }
        return organizationListResDTOList;
    }

    public OrganizationShowResDTO transferOrganizationShowResDTO(OrganizationDTO organizationDTO, OrganizationDTO parentOrganizationDTO) {
        OrganizationShowResDTO organizationShowResDTO = new OrganizationShowResDTO();
        BeanKit.copyProperties(organizationDTO, organizationShowResDTO);
        if (Func.isNotEmpty(parentOrganizationDTO)) {
            organizationShowResDTO.setPname(parentOrganizationDTO.getName());
        }
        return organizationShowResDTO;
    }

    public OrganizationDTO transferOrganizationDTO(OrganizationModifyReqDTO organizationModifyReqDTO) {
        OrganizationDTO organizationDTO = new OrganizationDTO();
        BeanKit.copyProperties(organizationModifyReqDTO, organizationDTO);
        return organizationDTO;
    }

    public Wrapper<OrganizationEntity> transferOrganizationEntityWrapper(OrganizationModifyReqDTO organizationModifyReqDTO) {
        Wrapper<OrganizationEntity> entityWrapper = new EntityWrapper<>();
        entityWrapper.isWhere(Boolean.FALSE);

        if (Func.isNotEmpty(organizationModifyReqDTO.getName())) {
            entityWrapper.eq(OrganizationEntity.DB_COL_NAME, organizationModifyReqDTO.getName());
        }

        if (Func.isNotEmpty(organizationModifyReqDTO.getCode())) {
            entityWrapper.or(OrganizationEntity.DB_COL_CODE + StrKit.S_SPACE + StrKit.S_EQUAL + StrKit.S_SPACE + "{0}", organizationModifyReqDTO.getCode());
        }

        entityWrapper.and().ne(OrganizationEntity.DB_COL_ID, organizationModifyReqDTO.getId());
        return entityWrapper;
    }

    public boolean validateOrganizationModifyReqDTO(OrganizationModifyReqDTO organizationModifyReqDTO) {
        if (Func.isEmpty(organizationModifyReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }
        return true;
    }

    public Wrapper<OrganizationEntity> transferOrganizationEntityWrapper(OrganizationAddReqDTO organizationAddReqDTO) {
        Wrapper<OrganizationEntity> entityWrapper = new EntityWrapper<>();
        if (Func.isNotEmpty(organizationAddReqDTO.getName())) {
            entityWrapper.eq(OrganizationEntity.DB_COL_NAME, organizationAddReqDTO.getName());
        }

        if (Func.isNotEmpty(organizationAddReqDTO.getCode())) {
            entityWrapper.isWhere(Boolean.FALSE).or(OrganizationEntity.DB_COL_CODE + StrKit.S_SPACE + StrKit.S_EQUAL + StrKit.S_SPACE + "{0}", organizationAddReqDTO.getCode());
        }

        return entityWrapper;
    }

    public OrganizationDTO transferOrganizationDTO(OrganizationAddReqDTO organizationAddReqDTO) {
        OrganizationDTO organizationDTO = new OrganizationDTO();
        BeanKit.copyProperties(organizationAddReqDTO, organizationDTO);
        return organizationDTO;
    }

    public boolean validateOrganizationAddReqDTO(OrganizationAddReqDTO organizationAddReqDTO) {
        if (Func.isEmpty(organizationAddReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }
        return true;
    }
}
