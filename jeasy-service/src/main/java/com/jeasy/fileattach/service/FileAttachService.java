package com.jeasy.fileattach.service;

import com.baomidou.mybatisplus.plugins.Page;
import com.jeasy.base.service.BaseService;
import com.jeasy.fileattach.dto.*;

import java.util.List;

/**
 * 文件附件 Service
 *
 * @author taomk
 * @version 1.0
 * @since 2017/11/08 16:33
 */
public interface FileAttachService extends BaseService<FileAttachDTO> {

    /**
     * 列表
     *
     * @param fileattachListReqDTO 入参DTO
     * @return
     */
    List<FileAttachListResDTO> list(FileAttachListReqDTO fileattachListReqDTO);

    /**
     * 列表Version1
     *
     * @param fileattachListReqDTO 入参DTO
     * @return
     */
    List<FileAttachListResDTO> listByVersion1(FileAttachListReqDTO fileattachListReqDTO);

    /**
     * 列表Version2
     *
     * @param fileattachListReqDTO 入参DTO
     * @return
     */
    List<FileAttachListResDTO> listByVersion2(FileAttachListReqDTO fileattachListReqDTO);

    /**
     * 列表Version3
     *
     * @param fileattachListReqDTO 入参DTO
     * @return
     */
    List<FileAttachListResDTO> listByVersion3(FileAttachListReqDTO fileattachListReqDTO);

    /**
     * First查询
     *
     * @param fileattachListReqDTO 入参DTO
     * @return
     */
    FileAttachListResDTO listOne(FileAttachListReqDTO fileattachListReqDTO);

    /**
     * 分页
     *
     * @param fileattachPageReqDTO 入参DTO
     * @param currentPage 当前页
     * @param pageLimit   每页大小
     * @return
     */
    Page<FileAttachPageResDTO> pagination(FileAttachPageReqDTO fileattachPageReqDTO, Integer currentPage, Integer pageLimit);

    /**
     * 新增
     *
     * @param fileattachAddReqDTO 入参DTO
     * @return
     */
    Boolean add(FileAttachAddReqDTO fileattachAddReqDTO);

    /**
     * 新增(所有字段)
     *
     * @param fileattachAddReqDTO 入参DTO
     * @return
     */
    Boolean addAllColumn(FileAttachAddReqDTO fileattachAddReqDTO);

    /**
     * 批量新增(所有字段)
     *
     * @param fileattachAddReqDTOList 入参DTO
     * @return
     */
    Boolean addBatchAllColumn(List<FileAttachAddReqDTO> fileattachAddReqDTOList);

    /**
     * 详情
     *
     * @param id 主键ID
     * @return
     */
    FileAttachShowResDTO show(Long id);

    /**
     * 批量详情
     *
     * @param ids 主键IDs
     * @return
     */
    List<FileAttachShowResDTO> showByIds(List<Long> ids);

    /**
     * 修改
     *
     * @param fileattachModifyReqDTO 入参DTO
     * @return
     */
    Boolean modify(FileAttachModifyReqDTO fileattachModifyReqDTO);

    /**
     * 修改(所有字段)
     *
     * @param fileattachModifyReqDTO 入参DTO
     * @return
     */
    Boolean modifyAllColumn(FileAttachModifyReqDTO fileattachModifyReqDTO);

    /**
     * 参数删除
     *
     * @param fileattachRemoveReqDTO 入参DTO
     * @return
     */
    Boolean removeByParams(FileAttachRemoveReqDTO fileattachRemoveReqDTO);
}
